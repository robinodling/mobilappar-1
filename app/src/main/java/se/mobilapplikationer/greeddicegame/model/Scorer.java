package se.mobilapplikationer.greeddicegame.model;

import java.util.ArrayList;

/**
 * Class with static methods for calculating score and checking for possible score
 */
class Scorer {


    private static boolean isStraight(ArrayList<Die> dices) {
        int[] mergedValues = mergeValues(dices);

        if (dices.size() < 6) {
            return false;
        }

        for (int i = 0; i < 6; i++) {
            if (mergedValues[i] != 1) {
                return false;
            }
        }

        return true;

    }

    /**
     * Checks if the dies could possibly score
     * @param dices to be checked
     * @return True if the dies have chance of scoring, else false
     */
    public static boolean hasPossibleScore(ArrayList<Die> dices) {

        int[] mergedValues = mergeValues(dices);

        for (Die d : dices) {
            //If 1 or 5
            if (d.getValue() == 1 || d.getValue() == 5) {
                return true;
            }
            //If three of a kind
            if (mergedValues[d.getValue() - 1] == 3) {
                return true;
            }
        }

        return isStraight(dices);

    }

    /**
     * Checks if the dies can obtain a score of  @score
     * @param dices the dies to be checked
     * @param score the score to be obtained
     * @return returns true if the dies can obtain the score, else false
     */
    public static boolean hasPossibleScore(ArrayList<Die> dices, int score) {
        return calculateScore(dices) >= score;
    }

    /**
     * Private method for creating a int array corresponding the number of values of the dies
     * @param dices dies to be merged
     * @return int array of length 6 with occurrences of values from dies [value1, value2, value3, value4, value5, value6]
     */
    private static int[] mergeValues(ArrayList<Die> dices) {

        int[] values = new int[6];

        for (Die d : dices) {
            switch (d.getValue()) {
                case 1:
                    values[0]++;
                    break;
                case 2:
                    values[1]++;
                    break;
                case 3:
                    values[2]++;
                    break;
                case 4:
                    values[3]++;
                    break;
                case 5:
                    values[4]++;
                    break;
                case 6:
                    values[5]++;
                    break;
            }
        }

        return values;

    }


    /**
     * Calculate score of selected dies
     * @param dices Dies to be calculated
     * @return returns the calculated score
     */
    public static int calculateScore(ArrayList<Die> dices) {

        int[] mergedValues = mergeValues(dices);
        int score = 0;

        //If dies are 6, check if they form a straight.
        if (dices.size() == 6) {
            if (isStraight(dices)) {
                score += 1000;
                return score;
            }
        }

        //For each possible die value
        for (int i = 0; i < mergedValues.length; i++) {

            //If the value occur 6 times it means two three of a kind
            if (mergedValues[i] == 6) {
                if (i == 0) {
                    return 2000;
                } else {
                    return (i + 1) * 2 * 100;
                }

            }
            //If theres equal or over three occurrences
            if (mergedValues[i] >= 3) {
                //Three of a kind ones
                if (i == 0) {
                    score += 1000;
                    //Remove three of a kind
                    mergedValues[i] -= 3;

                } else {
                    score += (i + 1) * 100;
                    //Remove three of a kind
                    mergedValues[i] -= 3;
                }

                if (mergedValues[i] > 0) {
                    //If theres still dies of value, run again
                    i--;
                }
                //Less than three
            } else {
                if (i  == 0) {
                    //If 1
                    score += mergedValues[i] * 100;
                } else if (i == 4) {
                    //if 5
                    score += mergedValues[i] * 50;
                }
            }


        }

        return score;
    }

    /**
     * Method for checking if all dice contribute to a score
     * @param dices
     * @return
     */
    public static boolean allDiceScored(ArrayList<Die> dices) {

        int[] mergedValues = mergeValues(dices);

        if (isStraight(dices)) {
            return true;
        }

        //Dies left
        int left = dices.size();

        for (int i = 0; i < mergedValues.length; i++) {


            if (left == 0) {
                return true;
            }

            if (left < 3) {
                if (i > 0) {
                    return mergedValues[4] == left;
                } else {
                    return mergedValues[0] + mergedValues[4] == left;
                }
            } else {
                if (i == 0 || i == 4) {
                    left -= mergedValues[i];
                } else {
                    if (mergedValues[i] != 3 && mergedValues[i] != 0) {

                        return false;
                    } else {
                        left -= mergedValues[i];
                    }
                }
            }
        }
        return true;

    }
}
