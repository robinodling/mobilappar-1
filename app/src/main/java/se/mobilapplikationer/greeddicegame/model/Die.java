package se.mobilapplikationer.greeddicegame.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;

/**
 * Class representing a single die
 */
public class Die extends ImageView implements View.OnClickListener, Parcelable {

    //The value of the die
    private int value;
    //Random value generator
    private final Random random = new Random();
    //Is the die stored?
    private boolean stored;// = false;
    //Is the die locked?
    private boolean locked;// = true;
    //Main context
    private final Context context;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(value);
        dest.writeByte((byte) (stored ? 1 : 0));
        dest.writeByte((byte) (locked ? 1 : 0));
        ContextHolder.setContext(context);

    }

    public Die(Context context) {

        super(context);
        this.context = context;
        init(context);

    }

    public Die(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(context);

    }

    public Die(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init(context);

    }

    /**
     * Sets the die image to nan and adds click listener.
     *
     * @param context
     */
    private void init(Context context) {
        setOnClickListener(this);
        setImageResource(context.getResources().getIdentifier("nan", "drawable", context.getPackageName()));
    }

    /**
     * Throw the die, generating a value from 1 to 6
     */
    public void throwDie() {
        this.value = random.nextInt((6 - 1) + 1) + 1;
        updateImage();
    }

    /**
     * Update the image to its corresponding value
     */
    private void updateImage() {
        setImageResource(context.getResources().getIdentifier("white" + value, "drawable", context.getPackageName()));
    }


    @Override
    public void onClick(View v) {
        //Dont do anything if the die is locked.
        if (locked) {
            return;
        }
        //If the die is only stored, unstore it.
        if (stored) {
            unStore();
        } else {
            store();
        }

    }


    private void store() {
        setImageResource(context.getResources().getIdentifier("grey" + value, "drawable", context.getPackageName()));
        this.stored = true;
    }

    private void unStore() {
        setImageResource(context.getResources().getIdentifier("white" + value, "drawable", context.getPackageName()));
        this.stored = false;
    }

    /**
     * Lock the die, setting the die image to red
     */
    public void lock() {
        setImageResource(context.getResources().getIdentifier("red" + value, "drawable", context.getPackageName()));
        this.locked = true;

    }

    /**
     *
     * @return stored condition of the die
     */
    public boolean isStored() {
        return stored;
    }

    /**
     *
     * @return Locked condition of the die
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     *
     * @return The value of the die
     */
    public int getValue() {
        return value;
    }

    /**
     * Reset the die, locking it, unstoring it and setting image to nan
     */
    public void reset() {
        this.value = 0;
        this.locked = true;
        this.stored = false;
        setImageResource(context.getResources().getIdentifier("nan", "drawable", context.getPackageName()));
    }

    /**
     * Unlocks the die
     */
    public void unlock() {
        this.locked = false;
    }



}
