package se.mobilapplikationer.greeddicegame.model;

import android.app.Activity;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;

import se.mobilapplikationer.greeddicegame.R;

/**
 * Main game class, controlling the game
 */
public class Game implements Parcelable {

    //Score limit of first turn
    private int firstRoundLimit;
    //Score limit to beat the game
    private int totalScoreLimit;
    //If any dice was locked
    private boolean diceWasLocked = false;
    //Save button
    private final Button saveButton;
    //Score button
    private final Button scoreButton;
    //If its the first throw on turn
    private boolean firstThrowOnTurn = true;
    //Total game score
    private int totalScore;
    //Total turn score
    private int turnScore;
    //Total number of turns
    private int totalTurns;
    //Set of 6 dies
    private Dice dice;
    //If the turn is started
    private boolean turnStarted = false;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(firstRoundLimit);
        dest.writeInt(totalScoreLimit);
        dest.writeByte((byte) (diceWasLocked ? 1 : 0));
        dest.writeByte((byte) (firstThrowOnTurn ? 1 : 0));
        dest.writeInt(totalScore);
        dest.writeInt(turnScore);
        dest.writeInt(totalTurns);
        dest.writeParcelable(dice, 0);
        dest.writeByte((byte) (turnStarted ? 1 : 0));
    }

    public void Game(Parcel in){
        this.firstRoundLimit = in.readInt();
        this.totalScoreLimit = in.readInt();
        this.diceWasLocked = in.readByte()!=0;
        this.firstThrowOnTurn = in.readByte()!=0;
        this.totalScore = in.readInt();
        this.turnScore = in.readInt();
        this.totalTurns = in.readInt();
        this.dice = in.readParcelable(Die.class.getClassLoader());
        this.turnStarted = in.readByte()!=0;
    }
    /**
     *
     * @param context main context
     * @param mainActivity main activity
     * @param total_score_limit total score limit to beat the game
     * @param first_throw_score_limit score limit for first throw on turn
     */
    public Game(Context context, Activity mainActivity, int total_score_limit, int first_throw_score_limit) {
        this.totalScore = 0;
        this.turnScore = 0;
        this.totalTurns = 0;
        this.firstRoundLimit = first_throw_score_limit;
        this.totalScoreLimit = total_score_limit;

        //Bind buttons to view
        saveButton = (Button) mainActivity.findViewById(R.id.save_button);
        scoreButton = (Button) mainActivity.findViewById(R.id.score_button);

        this.dice = new Dice(context, mainActivity);

    }




    /**
     *
     * @return total score limit
     */
    public int getTotalScoreLimit() {
        return totalScoreLimit;
    }

    /**
     *
     * @return first throw on turn limit
     */
    public int getFirstRoundLimit() {
        return firstRoundLimit;
    }

    /**
     * Set the save button listener
     * @param listener
     */
    public void setSaveButtonOnClickListener(View.OnClickListener listener) {
        saveButton.setOnClickListener(listener);
    }

    /**
     * Set the score button listener
     * @param listener
     */
    public void setScoreButtonOnClickListener(View.OnClickListener listener) {
        scoreButton.setOnClickListener(listener);
    }

    /**
     * Throw all unlocked dies
     */
    public void throwDice() {

        dice.throwDice();
        diceWasLocked = false;

    }

    /**
     *
     * @return Total score
     */
    public int getTotalScore() {
        return totalScore;
    }

    /**
     *
     * @return Total turn score
     */
    public int getTurnScore() {
        return turnScore;
    }

    /**
     *
     * @return true if all free dies have a possible score
     */
    public boolean hasPossibleScore() {
        return Scorer.hasPossibleScore(dice.getFreeDice());
    }

    /**
     * Update turn score with the score from all the stored dice
     */
    public void calculateTurnScore() {
        turnScore += Scorer.calculateScore(dice.getStoredDice());
    }

    /**
     * Start turn
     */
    public void startTurn() {
        this.turnStarted = true;
        dice.resetDice();
        dice.startTurn();
    }

    /**
     *
     * @return True if the turn is started, else false
     */
    public boolean isTurnStarted() {
        return turnStarted;
    }

    /**
     * End turn, resetting dies, updating score
     * @param busted if the player busted or not
     */
    public void endTurn(boolean busted) {


        //dice.resetDice();
        dice.lockAllDies();
        //If the player did not bust, add turn score to total score
        if (!busted) {
            this.totalScore += this.turnScore;
        }

        firstThrowOnTurn = true;
        turnStarted = false;
        turnScore = 0;

    }

    /**
     * Restart turn, reset dies but not resetting turn score
     */
    public void restartTurn() {

        dice.resetDice();
        turnStarted = false;
        firstThrowOnTurn = false;
        System.out.println("restartTurn Settings firstThrowOnTurn: " + firstThrowOnTurn);

    }

    /**
     * Save all stored dies
     */
    public void saveDice() {
        diceWasLocked = true;
        dice.lockDice();

    }

    /**
     *
     * @return true if a dice was locked, else false
     */
    public boolean diceWasLocked() {
        return diceWasLocked;
    }

    /**
     *
     * @return True if all stored dies can score together
     */
    public boolean allDiceScored() {
        return Scorer.allDiceScored(dice.getStoredDice());
    }

    /**
     *
     * @return Number of dies that are locked
     */
    public int getNumberOfLockedDice() {
        return dice.getLockedDice().size();
    }

    /**
     *
     * @return Total number of turns played
     */
    public int getTotalTurns() {
        return totalTurns;
    }

    /**
     * Increment turns by 1
     * @return Current total score after increment
     */
    public int incrementTurns() {
        this.totalTurns++;

        return this.totalTurns;
    }

    /**
     *
     * @return True if its the first throw on turn, else false
     */
    public boolean isFirstThrowOnTurn() {
        return firstThrowOnTurn;
    }

    /**
     * Set if its the first throw on turn
     * @param firstThrowOnTurn
     */
    public void setFirstThrowOnTurn(boolean firstThrowOnTurn) {
        this.firstThrowOnTurn = firstThrowOnTurn;
    }

    /**
     * Method for checking if all dies can obtain a score of @score
     * @param score The score to check if all dies can obtain
     * @return True if dies can obtain the score stated, else false
     */
    public boolean hasPossibleScore(int score) {
        return Scorer.hasPossibleScore(dice.getAllDice(), score);
    }

    /**
     * Calculate score of all dies
     * @return The score of all dies
     */
    public int calculateAll() {
        return Scorer.calculateScore(dice.getAllDice());
    }

}
