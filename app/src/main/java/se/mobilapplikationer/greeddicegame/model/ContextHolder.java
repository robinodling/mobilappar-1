package se.mobilapplikationer.greeddicegame.model;

import android.content.Context;

/**
 * Created by Robin on 2015-08-17.
 */
public class ContextHolder {
    
    public static Context context;
    public static void setContext(Context context){
        ContextHolder.context = context;
    }
    public static Context getContext(){
        return ContextHolder.context;
    }
    
}
