package se.mobilapplikationer.greeddicegame.model;

import android.app.Activity;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Dice
 *
 * Class to represent a set of 6 dice
 *
 *
 *
 */
class Dice implements Parcelable {

    private Die[] dice = new Die[6];



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelableArray(dice, 0);
    }


    public Dice(Parcel in){
        this.dice = (Die[]) in.readParcelableArray(Die.class.getClassLoader());
    }

    /**
     * Creates 6 dies
     *
     * @param context The main context
     * @param gameActivity the game activity to be able to find die id
     */
    public Dice(Context context, Activity gameActivity) {
        for (int i = 0; i < 6; i++) {

            int dieId = context.getResources().getIdentifier("die" + String.valueOf(i + 1), "id", context.getPackageName());
            dice[i] = (Die) gameActivity.findViewById(dieId);

        }


    }


    /**
     * Locks all stored dies
     */
    public void lockDice() {

        ArrayList<Die> lockedDice = new ArrayList<Die>();

        for (Die d : dice) {
            if (d.isStored()) {
                d.lock();
                lockedDice.add(d);
            }
        }

    }

    /**
     * Throws all unlocked dies
     */
    public void throwDice() {
        for (Die d : dice) {
            if (!d.isLocked()) {
                d.throwDie();
            }
        }
    }

    /**
     * Returns all dies
     * @return
     */
    public ArrayList<Die> getAllDice() {
        ArrayList<Die> res = new ArrayList<Die>();
        for(Die d : dice){
            res.add(d);
        }
        return res;
    }

    /**
     * Returns all dies that are stored but not locked
     * @return ArrayList of Die containing all stored but not locked dies
     */
    public ArrayList<Die> getStoredDice() {
        ArrayList<Die> storedDice = new ArrayList<Die>();

        for (Die d : dice) {
            if (d.isLocked()) {
                continue;
            }
            if (d.isStored()) {
                storedDice.add(d);
            }
        }


        return storedDice;

    }

    /**
     * Method for retreiving all dies which are not locked or stored
     *
     * @return
     */
    public ArrayList<Die> getFreeDice() {
        ArrayList<Die> freeDice = new ArrayList<Die>();

        for (Die d : dice) {
            if (d.isLocked() || d.isStored()) {
                continue;
            } else {
                freeDice.add(d);
            }


        }

        return freeDice;
    }

    /**
     * Resets all dies, locking them and setting their picture to nan
     */
    public void resetDice() {
        for (Die d : dice) {
            d.reset();
        }
    }

    /**
     * Method for retreiving all locked dies
     * @return
     */
    public ArrayList<Die> getLockedDice() {
        ArrayList<Die> lockedDice = new ArrayList<Die>();

        for (Die d : dice) {
            if (d.isLocked()) {
                lockedDice.add(d);
            }


        }

        return lockedDice;
    }

    /**
     * Method for starting turn. Unlocks all dies
     */
    public void startTurn() {

        for (Die d : dice) {
            d.unlock();
        }

    }

    public void lockAllDies() {
        for (Die d : dice) {
            d.lock();
        }
    }

}
