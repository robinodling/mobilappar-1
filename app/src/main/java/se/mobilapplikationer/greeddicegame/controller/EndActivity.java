package se.mobilapplikationer.greeddicegame.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import se.mobilapplikationer.greeddicegame.R;


public class EndActivity extends Activity {

    //Text view to show score
    private TextView scoreMessage;
    //Try again button
    private Button tryAgainButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        tryAgainButton = (Button) findViewById(R.id.try_again_button);
        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restartGame();
            }
        });

        scoreMessage = (TextView) findViewById(R.id.score_message);

        //Receive total score and total turns from game activity
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int total_turns = extras.getInt("TOTAL_TURNS");
            int total_score = extras.getInt("TOTAL_SCORE");

            //Set the end-game message
            scoreMessage.setText("You beat the game in " + total_turns
                    + " turns with a total score of " + total_score + " points!");

        } else {
            scoreMessage.setText("Good job!");
        }


    }

    private void restartGame() {
        //Start the game again
        Intent intent = new Intent(this, StartActivity.class);
        startActivity(intent);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
