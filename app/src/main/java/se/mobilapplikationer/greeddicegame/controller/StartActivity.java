package se.mobilapplikationer.greeddicegame.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import se.mobilapplikationer.greeddicegame.R;


public class StartActivity extends Activity {


    //Startscreen buttons
    private Button startButton;
    private Button endButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        startButton = (Button) findViewById(R.id.start_button);
        endButton = (Button) findViewById(R.id.end_button);
        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endGame();
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame();
            }
        });

    }

    private void endGame() {

        finish();
    }

    private void startGame() {

        //When starting game, show dialogue box alowing the player to state the game settings.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Game settings");

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final EditText total_score_limit_input = new EditText(this);
        total_score_limit_input.setHint("Total score limit");
        total_score_limit_input.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(total_score_limit_input);

        final EditText first_throw_limit_input = new EditText(this);
        first_throw_limit_input.setHint("First throw score limit");
        first_throw_limit_input.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(first_throw_limit_input);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int total_score_limit = Integer.valueOf(total_score_limit_input.getText().toString());
                int first_round_limit = Integer.valueOf(first_throw_limit_input.getText().toString());

                startGameIntent(total_score_limit, first_round_limit);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setView(layout);
        builder.show();

    }

    private void startGameIntent(int total_score_limit, int first_throw_limit) {
        Intent intent = new Intent(this, GameActivity.class);

        intent.putExtra("TOTAL_SCORE_LIMIT", total_score_limit);
        intent.putExtra("FIRST_THROW_SCORE_LIMIT", first_throw_limit);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
