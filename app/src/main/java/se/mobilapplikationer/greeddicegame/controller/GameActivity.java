package se.mobilapplikationer.greeddicegame.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import se.mobilapplikationer.greeddicegame.R;
import se.mobilapplikationer.greeddicegame.model.Game;

/**
 * Created by Robin on 2015-06-30.
 */
public class GameActivity extends Activity {

    //The game controller
    private Game diceGame;

    //Text views to show numbers
    private TextView scoreText;
    private TextView turnScoreText;
    private TextView totalTurnText;

    //Throw dice button
    private Button throwButton;


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putParcelable("DICE_GAME", diceGame);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);


        this.scoreText = (TextView) findViewById(R.id.total_score);
        this.turnScoreText = (TextView) findViewById(R.id.turn_score);
        this.totalTurnText = (TextView) findViewById(R.id.total_turns);



        if(savedInstanceState != null){
            this.diceGame = savedInstanceState.getParcelable("DICE_GAME");


            this.scoreText = (TextView) findViewById(R.id.total_score);
            this.turnScoreText = (TextView) findViewById(R.id.turn_score);
            this.totalTurnText = (TextView) findViewById(R.id.total_turns);

            this.scoreText.setText(String.valueOf(this.diceGame.getTotalScore()));
            this.turnScoreText.setText(String.valueOf(this.diceGame.getTurnScore()));
            this.totalTurnText.setText(String.valueOf(this.diceGame.getTotalTurns()));



        }else{
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                diceGame = new Game(getApplicationContext(), this, extras.getInt("TOTAL_SCORE_LIMIT"), extras.getInt("FIRST_THROW_SCORE_LIMIT"));
            }

            //Initialize numbers
            scoreText.setText("0");
            turnScoreText.setText("0");
            totalTurnText.setText("0");

        }



        //Receive limits from start-screen dialogue

        diceGame.setSaveButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //If the turn havent started, you cannot save any score
                if (!diceGame.isTurnStarted()) {
                    Toast.makeText(getApplicationContext(), "Turn not started. Start turn by throwing dice", Toast.LENGTH_SHORT).show();

                    return;
                }

                //If all stored dies scored
                if (diceGame.allDiceScored()) {

                    //Calculate score and save dies
                    diceGame.calculateTurnScore();
                    turnScoreText.setText(String.valueOf(diceGame.getTurnScore()));
                    diceGame.saveDice();

                    //If all dies are locked, restart turn without resetting turn score
                    if (diceGame.getNumberOfLockedDice() == 6) {
                        Toast.makeText(getApplicationContext(), "All dice used, restarting turn with score stored", Toast.LENGTH_SHORT).show();
                        diceGame.restartTurn();
                    }

                } else {
                    //If all stored dies didnt score, let player try again
                    Toast.makeText(getApplicationContext(), "Choose scoring die/dice", Toast.LENGTH_SHORT).show();
                }

            }
        });


        diceGame.setScoreButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //If the turn have not started
                if (!diceGame.isTurnStarted()) {
                    //If its the first throw this turn, you cannot set score
                    if (diceGame.isFirstThrowOnTurn()) {
                        Toast.makeText(getApplicationContext(), "Turn not started. Start turn by throwing dice", Toast.LENGTH_SHORT).show();
                        return;
                    }


                }

                //If turn score equals 0, you cannot score any points
                if (diceGame.getTurnScore() == 0) {
                    Toast.makeText(getApplicationContext(), "No turnscore to add to total", Toast.LENGTH_SHORT).show();
                    return;
                }

                //End turn and update total score
                endTurn(false);
                scoreText.setText(String.valueOf(diceGame.getTotalScore()));


            }
        });

        throwButton = (Button) findViewById(R.id.throw_button);
        throwButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //If the turn is not started
                if (!diceGame.isTurnStarted()) {

                    //Start turn and throw dies
                    diceGame.startTurn();
                    diceGame.throwDice();

                    //If its the first throw on turn, check if the dies can score up to the limit player stated, if not, start new turn
                    if (diceGame.isFirstThrowOnTurn()) {
                        diceGame.setFirstThrowOnTurn(false);
                        System.out.println(String.valueOf(diceGame.calculateAll()));
                        if (!diceGame.hasPossibleScore(diceGame.getFirstRoundLimit())) {
                            Toast.makeText(getApplicationContext(), "Throw did not give dices with points equal or above: " + String.valueOf(diceGame.getFirstRoundLimit()), Toast.LENGTH_SHORT).show();
                            endTurn(true);
                            return;
                        }
                    }

                    //Update turn counter
                    totalTurnText.setText(String.valueOf(diceGame.getTotalTurns()));

                    //If the stored dice thrown have no possible score, player busts, start new turn
                    if (!diceGame.hasPossibleScore()) {
                        Toast.makeText(getApplicationContext(), "BUSTED", Toast.LENGTH_SHORT).show();

                        endTurn(true);
                        return;
                    }

                    return;
                }

                //If no dice was locked, tell player to lock a die before throwing again.
                if (!diceGame.diceWasLocked()) {
                    Toast.makeText(getApplicationContext(), "Please save a scoring die", Toast.LENGTH_SHORT).show();
                    return;
                }

                diceGame.throwDice();

                if (!diceGame.hasPossibleScore()) {
                    Toast.makeText(getApplicationContext(), "BUSTED", Toast.LENGTH_SHORT).show();

                    endTurn(true);
                }

            }
        });


    }

    private void endTurn(boolean busted) {

        diceGame.endTurn(busted);
        totalTurnText.setText(String.valueOf(diceGame.incrementTurns()));

        //If the total score is >= the total score limit, player wins, start end game activity
        if (diceGame.getTotalScore() >= diceGame.getTotalScoreLimit()) {
            Intent intent = new Intent(this, EndActivity.class);
            intent.putExtra("TOTAL_TURNS", diceGame.getTotalTurns());
            intent.putExtra("TOTAL_SCORE", diceGame.getTotalScore());
            startActivity(intent);
        }

        scoreText.setText(String.valueOf(diceGame.getTotalScore()));
        turnScoreText.setText(String.valueOf("0"));

    }
}